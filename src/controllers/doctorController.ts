import { RequestHandler } from "express"
import { doctorModel } from "../models/doctorModel"

class DoctorController {

    getAll: RequestHandler = (req, res) => {
        res.json(doctorModel.getAll())
    }

}
export const doctorController = new DoctorController()