enum WorkTime {
    morning = 'morning',
    night = 'night'
}

interface IDoctor {
    id: string
    name: string
    age: number
    isDoctor: boolean
    workTime: WorkTime
}

const doctors: IDoctor[] = [
    {
        id: "1",
        name: 'ali',
        age: 20,
        isDoctor: true,
        workTime: WorkTime.morning
    },
    {
        id: "2",
        name: 'reza',
        age: 25,
        isDoctor: false,
        workTime: WorkTime.night
    },
    {
        id: "3",
        name: 'seyed',
        age: 30,
        isDoctor: false,
        workTime: WorkTime.morning
    },
]

class DoctorModel {
    private db: IDoctor[]

    constructor(doctors: IDoctor[]) {
        this.db = doctors
    }

    add = (doctorData: IDoctor) => {
        this.db.push(doctorData)
        return {
            message: 'data added successfully'
        }
    }

    getAll = () => this.db

    getOne = (id: string) => this.db.find((doctor) => doctor.id === id)

    remove = (id: string) => {
        const doctorIndex = this.db.findIndex((doctor) => doctor.id === id)
        if (doctorIndex < 0) {
            return { message: 'Doctor not found' }
        }
        return this.db.splice(doctorIndex)
    }

    update = (id: string, modificationData: Partial<IDoctor>) => {
        const itemToModify = this.db.find(doctor => doctor.id === id)
        const itemIndex = this.db.findIndex(doctor => doctor.id === id)

        let newItem = {}

        if (itemToModify) {


            Object.keys(modificationData).forEach(property => {
                if (!(property in itemToModify)) return;
                
            })
        }

        this.db.splice(itemIndex, 1,)
    }
}

export const doctorModel = new DoctorModel(doctors)