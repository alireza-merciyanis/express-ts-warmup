import express from "express";
import { doctorController } from "../controllers/doctorController";
import doctorRouter from "./doctorRouter";

const mainRouter = express.Router()

// Global middleware for "/doctor"
// mainRouter.use((req, res, next) => {
//     console.log('test router middleware')
//     next()
// })()

// Routers
mainRouter.use('/doctors', doctorRouter)


export default mainRouter