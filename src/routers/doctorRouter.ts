import express from "express";
import { doctorController } from "../controllers/doctorController";

const doctorRouter = express.Router()

// '/doctor' routes and methods
doctorRouter.get('/', doctorController.getAll)


export default doctorRouter